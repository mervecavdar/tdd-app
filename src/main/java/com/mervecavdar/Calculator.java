package com.mervecavdar;

public class Calculator {

    private final boolean status;

    public Calculator() {
        this.status = true;
    }

    public boolean getStatus() {
        return status;
    }

    public int addition(int number1, int number2) {
        return number1 + number2;
    }

    public int division(int number1, int number2) {
        if (number2 == 0) {
            throw new IllegalArgumentException("Cannot division by zero.");
        }
        return number1 / number2;
    }

}
