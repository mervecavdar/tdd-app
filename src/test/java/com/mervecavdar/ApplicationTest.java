package com.mervecavdar;

import com.mervecavdar.controller.Controller;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class ApplicationTest {

    @Autowired
    private Controller controller;

    @Test
    public void contextLoads() {
        assertThat(controller).isNull();
    }

}
