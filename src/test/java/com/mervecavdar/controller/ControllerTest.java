package com.mervecavdar.controller;

import org.junit.Test;
import org.mockito.Mock;
import org.springframework.boot.test.web.client.TestRestTemplate;

import static org.assertj.core.api.Assertions.assertThat;

public class ControllerTest {

    private static final String URL = "http://localhost:8080";

    @Mock
    private final TestRestTemplate testRestTemplate = new TestRestTemplate();

    @Test
    public void homeResponse() {
        String body = testRestTemplate.getForObject(URL + "/", String.class);
        assertThat(body).isEqualTo("Home Page");
    }

}